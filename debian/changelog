imgvtopgm (2.1-2) unstable; urgency=medium

  * Team upload.
  * Team maintenance by Debian Phototools Team
  * Short debhelper
  * DEP5 copyright
  * Standards-Version: 4.6.2 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Update watch file format version to 4.
  * Hardening

 -- Andreas Tille <tille@debian.org>  Wed, 11 Jan 2023 18:00:20 +0100

imgvtopgm (2.1-1) unstable; urgency=low

  * New upstream version, make it compile for netpbm11 (Closes: #1012633)
  * Thanks Adrian for the NMU (Closes: #999003)
  * removed Recommends, the packages were removed
  * Switch to dpkg-source 3.0 (quilt) format (Closes: #1007585)
  * Fix FTCBFS: Export cross tools for configure. Thanks Helmut (Closes: #989953)
  * debian/control:
    + Bumped Standards-Version to 4.5.1, no changes needed
  * Increased debhelper version to 13
  * Fixed lintian warning about national-encoding in debian/man/*.de.1

 -- Erik Schanze <eriks@debian.org>  Fri, 02 Dec 2022 00:17:28 +0100

imgvtopgm (2.0-9.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/rules: Add build-{arch,indep}. (Closes: #999003)

 -- Adrian Bunk <bunk@debian.org>  Mon, 27 Dec 2021 21:04:01 +0200

imgvtopgm (2.0-9) unstable; urgency=low

  * debian/control:
    + remove Kpilot from Recommends
    + Bumped Standards-Version to 3.9.1, no changes needed
  * Increased debhelper version to 8
  * Updated Homepage in debian/control, debian/copyright and
    debian/watch

 -- Erik Schanze <eriks@debian.org>  Sat, 12 Feb 2011 12:15:44 +0100

imgvtopgm (2.0-8) unstable; urgency=low

  * debian/rules:
    - Added handling of DEB_BUILD_OPTIONS (Closes: #437216)
    - changed distclean rule (lintian warning)
  * Bumped Standards-Version to 3.7.2, no changes needed

 -- Erik Schanze <eriks@debian.org>  Sat, 11 Aug 2007 21:49:28 +0200

imgvtopgm (2.0-7) unstable; urgency=low

  * Fixed watch file
  * Changed priority back to optional

 -- Erik Schanze <eriks@debian.org>  Sat, 22 Apr 2006 21:35:19 +0200

imgvtopgm (2.0-6) unstable; urgency=low

  * Changed maintainers email address
  * Changed german umlauts in manpages to groff sequences
  * Updated to Standards-Version 3.6.2, no changes needed
  * Changed priority to extra, requested by Debian Installer
  * Updated watch file
  * Removed version in NAME line of manpages (Closes: #340475)
  * Removed AUTHORS, we have this in debian/copyright
  * Added homepage and improved description in debian/control
  * Improved copyright file

 -- Erik Schanze <eriks@debian.org>  Sun,  9 Apr 2006 15:52:38 +0200

imgvtopgm (2.0-5) unstable; urgency=low

  * New maintainer, closes: #261584
  * Remove directory debian.old/
  * Remove unused debian/README.[d|D]ebian
  * Update to Standards-Version 3.6.1
    + Modify debian/copyright
    + Removed emacs stuff from this file
    + Remove dh_suidregister from debian/rules
    + Removed INSTALL and (useless) NEWS from debian/docs
    + Remove unneeded debian/dirs
  * Updated download URL in debian/copright
  * Translated man pages into German
    + Stored under debian/man/ and modify debian/rules for installing
  * Modify debian/rules to install sample/* into packages example diretory
  * Added debian/watch file
  * Change default VIEWER to xli in imgvview (it is the smallest one)
  * Added depends on xli in debian/control

 -- Erik Schanze <schanzi_@gmx.de>  Sat, 31 Jul 2004 21:23:49 +0200

imgvtopgm (2.0-4) unstable; urgency=low

  * Orphaned this package.

 -- John Goerzen <jgoerzen@complete.org>  Mon, 26 Jul 2004 15:06:19 -0500

imgvtopgm (2.0-3) unstable; urgency=low

  * Due to changes in netpbm packages, reversed the Debian patch to
    configure.  Closes: #211255.

 -- John Goerzen <jgoerzen@complete.org>  Thu,  9 Oct 2003 16:50:27 -0500

imgvtopgm (2.0-2) unstable; urgency=low

  * ACK NMU.  Closes: #93953, #91504.
  * Updated Standards-Version.  Closes: #87168.
  * Closing out other fixed bugs.  Closes: #91177.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 24 Apr 2001 14:01:50 -0500

imgvtopgm (2.0-1.1) unstable; urgency=low

  * NMU
  * Rebuilt with newer debhelper (closes: bug#91504)
  * Added Build-Depends.
  * Added mandir=/usr/share/man

 -- Marcelo E. Magallon <mmagallo@debian.org>  Fri, 13 Apr 2001 20:23:04 +0200

imgvtopgm (2.0-1) unstable; urgency=low

  * New upstream release, closes bug #38443

 -- John Goerzen <jgoerzen@complete.org>  Tue, 22 Jun 1999 06:22:38 -0500

imgvtopgm (1.3-2) unstable; urgency=low

  * Added #include <string.h> to slop.h, which should help compilation
    on glibc2.1 platforms, fixing bug #33215.
  * Changed control file maintainer e-mail to jgoerzen@complete.org.

 -- John Goerzen <jgoerzen@complete.org>  Mon, 15 Feb 1999 13:11:37 -0600

imgvtopgm (1.3-1) unstable; urgency=low

  * New upstream release
  * Closed bug #30016.  This was actually fixed in 1.2-2 but apparently
    that version never made it to the archive.

 -- John Goerzen <jgoerzen@complete.org>  Sun, 29 Nov 1998 11:34:00 -0600

imgvtopgm (1.2-2) unstable; urgency=low

  * Fixed copyright file
  * Now includes the samples from the upstream release

 -- John Goerzen <jgoerzen@complete.org>  Mon,  8 Jun 1998 15:48:18 -0500

imgvtopgm (1.2-1) unstable; urgency=low

  * Initial Release.

 -- John Goerzen <jgoerzen@complete.org>  Sun,  7 Jun 1998 15:49:00 -0500
